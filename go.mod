module bitbucket.org/GottySG36/cluster_pangenome

go 1.13

require (
	bitbucket.org/GottySG36/graph v0.1.3
	bitbucket.org/GottySG36/pangenome v0.4.0
)
