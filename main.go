// Utility to create graphs from a list of edges and extract the sub-graphs

package main

import (
    "flag"
    "log"

    "bitbucket.org/GottySG36/graph"
    "bitbucket.org/GottySG36/pangenome"
)

var inp     = flag.String("i", "",
    `Input file to use to build graph. File has to be tabulated.
Each line represent an edge in the graph and must contain 
at least two columns. Only the first two are used though 
(origin and end). Everything else is discarded.`,
)
var outd  = flag.String("o", "", "Output directory in which to write the results")
var pan     = flag.String("p", "", "Pangenome file to parse and clusterize")
var core    = flag.Float64("core", 95, "Core genes threshold")
var acc     = flag.Float64("accessory", 15, "Accessory genes threshold")

func main() {
    flag.Parse()

    g, err := graph.GraphFromFile(*inp)
    if err != nil {
        log.Fatalf("Error -:- GraphFromFile : %v\n", err)
    }
    subGraphs := g.GetSubGraphs()
    conv := pangenome.ConversionTable(subGraphs)

    pang, tax, err := pangenome.LoadPangenome(*pan)
    if err != nil {
        log.Fatalf("Error -:- LoadPangenome : %v\n", err)
    }

    pangCl, err := pang.Cluster(conv, *core, *acc)
    if err != nil {
        log.Fatalf("Error -:- Cluster : %v\n", err)
    }

    summ := pangCl.Summarize()

    if *outd != ""{
        // Writing pangenome to file
        err = pangCl.Write(*outd, tax)
        if err != nil {
            log.Fatalf("Error -:- Write pangenome : %v\n", err)
        }

        // Writing summary to file
        err = summ.Write(*outd)
        if err != nil {
            log.Fatalf("Error -:- Write summary : %v\n", err)
        }
        clList := conv.GetClusters()
        err := clList.Write(*outd)
        if err != nil {
            log.Fatalf("Error -:- Write clusters : %v\n", err)
        }

    } else {
        summ.Print()
    }

}

